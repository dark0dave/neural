package com.personal.neural;

import static java.math.BigDecimal.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.core.Is.is;

import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;

public class NetTest {
  private final Net net = new Net();

  @Before
  public void setUp() {
    final BigDecimal[][] trainingDataSet = new BigDecimal[4][3];
    trainingDataSet[0] = new BigDecimal[] {ZERO, ZERO, ONE};
    trainingDataSet[1] = new BigDecimal[] {ONE, ONE, ONE};
    trainingDataSet[2] = new BigDecimal[] {ONE, ZERO, ONE};
    trainingDataSet[3] = new BigDecimal[] {ZERO, ONE, ONE};

    final BigDecimal[][] expectedResult = new BigDecimal[4][1];
    expectedResult[0][0] = ZERO;
    expectedResult[1][0] = ONE;
    expectedResult[2][0] = ONE;
    expectedResult[3][0] = ZERO;

    net.train(trainingDataSet, expectedResult, 1000L);
  }

  @Test
  public void train() {
    final BigDecimal[][] predict = new BigDecimal[][] {{ONE, ONE, ONE}};

    final BigDecimal[][] result = net.predict(predict);
    assertThat(result[0][0], is(closeTo(ONE, valueOf(0.01))));
  }
}
