package com.personal.neural;

import static java.math.BigDecimal.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

import java.math.BigDecimal;
import org.junit.Test;

public class HelpersTest {
  Helpers helpers = new Helpers();

  @Test
  public void sigmoidDefaultsToZero() {
    final BigDecimal zero = Helpers.sigmoid(ZERO);
    assertThat(zero, is(equalTo(ZERO)));

    final BigDecimal verySmallValue = Helpers.sigmoid(BigDecimal.valueOf(-500L));
    assertThat(verySmallValue, is(closeTo(ZERO, valueOf(1E-217))));
  }

  @Test
  public void sigmoid() {
    BigDecimal[][] testArray =
        new BigDecimal[][] {{valueOf(6), valueOf(1), valueOf(10), valueOf(0.5)}};
    BigDecimal[] result = Helpers.sigmoid(testArray)[0];
    assertThat(result[0], is(closeTo(ONE, valueOf(0.01))));
    assertThat(result[1], is(closeTo(valueOf(0.73), valueOf(0.01))));
    assertThat(result[2], is(closeTo(ONE, valueOf(0.01))));
    assertThat(result[3], is(closeTo(valueOf(0.62), valueOf(0.01))));
  }

  @Test
  public void sigmoidDerivative() {
    BigDecimal[][] testArray =
        new BigDecimal[][] {{valueOf(6), valueOf(6), valueOf(6), valueOf(6)}};
    BigDecimal[][] result = Helpers.sigmoidDerivative(testArray);
    BigDecimal[][] expectedResult =
        new BigDecimal[][] {{valueOf(-30), valueOf(-30), valueOf(-30), valueOf(-30)}};
    assertThat(result, is(equalTo(expectedResult)));
  }

  @Test
  public void transpose() {
    BigDecimal[][] testArray = new BigDecimal[4][4];
    testArray[0] = new BigDecimal[] {ZERO, ONE, valueOf(2), valueOf(3)};
    BigDecimal[][] result = Helpers.transpose(testArray);
    BigDecimal[][] expectedResult = new BigDecimal[4][4];
    expectedResult[0][0] = ZERO;
    expectedResult[1][0] = ONE;
    expectedResult[2][0] = valueOf(2);
    expectedResult[3][0] = valueOf(3);
    assertThat(result, is(equalTo(expectedResult)));
  }

  @Test(expected = IllegalArgumentException.class)
  public void matrixSubtractWithDifferentSizedMatrices() {
    BigDecimal[][] testArray1 = new BigDecimal[][] {{ZERO}};
    BigDecimal[][] testArray2 = new BigDecimal[][] {{ZERO}, {ZERO}};
    Helpers.matrixSubtract(testArray1, testArray2);
  }

  @Test(expected = IllegalArgumentException.class)
  public void dotProductWithDifferentSizedMatrices() {
    BigDecimal[][] testArray1 = new BigDecimal[][] {{ZERO}};
    BigDecimal[][] testArray2 = new BigDecimal[][] {{ZERO}, {ZERO}};
    Helpers.dotProduct(testArray1, testArray2);
  }
}
