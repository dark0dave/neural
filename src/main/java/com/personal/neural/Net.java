package com.personal.neural;

import static com.personal.neural.Helpers.*;

import java.math.BigDecimal;

class Net {
  private final Neuron neuron = new Neuron();

  void train(
      BigDecimal[][] trainingDataSet, BigDecimal[][] expectedResult, long numberOfIterations) {

    while (numberOfIterations-- > 0) {
      // Here we pass the training data to our neuron
      final BigDecimal[][] output = sigmoid(dotProduct(trainingDataSet, neuron.weight));

      // Here we find the difference between the expected result and the predicted output
      final BigDecimal[][] error = matrixSubtract(expectedResult, output);

      // Multiply the error by the input and the gradient of the Sigmoid curve from our output
      // This means less confident weights are adjusted more.
      final BigDecimal[][] adjustment =
          dotProduct(transpose(trainingDataSet), matrixMultiply(error, sigmoidDerivative(output)));

      // Adjust neuron weights
      neuron.weight = matrixAdd(neuron.weight, adjustment);
    }
  }

  BigDecimal[][] predict(BigDecimal[][] testDataSet) {
    return sigmoid(dotProduct(testDataSet, neuron.weight));
  }
}
