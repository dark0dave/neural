package com.personal.neural;

import java.math.BigDecimal;
import java.util.Random;

class Neuron {
  BigDecimal[][] weight = new BigDecimal[3][1];

  Neuron() {
    final Random random = new Random();
    random.setSeed(System.nanoTime());
    for (int i = 0; i < weight.length; i++) {
      weight[i][0] = BigDecimal.valueOf(random.nextDouble());
    }
  }
}
