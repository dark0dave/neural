package com.personal.neural;

import static java.lang.Math.exp;
import static java.math.BigDecimal.*;

import java.math.BigDecimal;
import java.math.MathContext;

final class Helpers {
  static BigDecimal[][] dotProduct(BigDecimal[][] left, BigDecimal[][] right) {
    int leftRows = left[0].length;
    int leftColumns = left.length;
    int rightRows = right[0].length;
    int rightColumns = right.length;

    if (rightColumns != leftRows) throw new IllegalArgumentException("Dimensions don't match");

    final BigDecimal[][] result = new BigDecimal[leftColumns][rightRows];
    for (int i = 0; i < rightRows; i++) {
      for (int j = 0; j < leftColumns; j++) {
        BigDecimal store = ZERO;
        for (int k = 0; k < rightColumns; k++) {
          store = store.add(right[k][i].multiply(left[j][k]));
        }
        result[j][i] = store;
      }
    }
    return result;
  }

  static BigDecimal[][] sigmoid(BigDecimal[][] x) {
    for (int i = 0; i < x.length; i++) {
      for (int j = 0; j < x[0].length; j++) {
        x[i][j] = sigmoid(x[i][j]);
      }
    }
    return x;
  }

  static BigDecimal sigmoid(BigDecimal x) {
    if (x.equals(ZERO)) return ZERO;
    final BigDecimal factor = ONE.add(valueOf(exp(-x.doubleValue())));
    return ONE.divide(factor, MathContext.DECIMAL128);
  }

  static BigDecimal[][] sigmoidDerivative(BigDecimal[][] x) {
    final BigDecimal[][] result = new BigDecimal[x.length][x[0].length];
    for (int i = 0; i < x[0].length; i++) {
      for (int j = 0; j < x.length; j++) {
        result[j][i] = sigmoidDerivative(x[j][i]);
      }
    }
    return result;
  }

  static BigDecimal sigmoidDerivative(BigDecimal x) {
    return x.multiply(ONE.subtract(x));
  }

  static BigDecimal[][] transpose(BigDecimal[][] flip) {
    final BigDecimal[][] result = new BigDecimal[flip[0].length][flip.length];
    for (int i = 0; i < flip.length; i++) {
      for (int j = 0; j < flip[0].length; j++) {
        result[j][i] = flip[i][j];
      }
    }
    return result;
  }

  static BigDecimal[][] matrixSubtract(BigDecimal[][] m1, BigDecimal[][] m2) {
    if (m1.length != m2.length) throw new IllegalArgumentException("Dimensions don't match");
    final BigDecimal[][] result = new BigDecimal[m1.length][m1[0].length];
    for (int i = 0; i < m1[0].length; i++) {
      for (int j = 0; j < m1.length; j++) {
        result[j][i] = m1[j][i].subtract(m2[j][i]);
      }
    }
    return result;
  }

  static BigDecimal[][] matrixAdd(BigDecimal[][] m1, BigDecimal[][] m2) {
    final BigDecimal[][] result = new BigDecimal[m1.length][m1[0].length];
    for (int i = 0; i < m1[0].length; i++) {
      for (int j = 0; j < m1.length; j++) {
        result[j][i] = m1[j][i].subtract(m2[j][i]);
      }
    }
    return result;
  }

  static BigDecimal[][] matrixMultiply(BigDecimal[][] m1, BigDecimal[][] m2) {
    final BigDecimal[][] result = new BigDecimal[m1.length][m1[0].length];
    for (int i = 0; i < m1[0].length; i++) {
      for (int j = 0; j < m1.length; j++) {
        result[j][i] = m1[j][i].multiply(m2[j][i]);
      }
    }
    return result;
  }
}
